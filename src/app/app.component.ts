import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public data: any = [];
  public displayData: any = [];
  public columns: any = [];
  public numberOfCols = 10;
  public currentPage = 1;
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('./assets/sample_data.json').subscribe(v => {
      this.data = v;
      console.log(this.data);
      // tslint:disable-next-line:max-line-length
      this.columns = [{key: 'id', disp: '#'}, {key: 'name', disp: 'Name'}, {key: 'phone', disp: 'Phone'}, {key: 'email', disp: 'Email'}, {key: 'company', disp: 'Company'}, {key: 'url', disp: 'URL'},
        // tslint:disable-next-line:max-line-length
        {key: 'status', disp: 'Status'}, {key: 'fee', disp: 'Fee'}, {key: 'guid', disp: 'GUID'}, {key: 'org_num', disp: 'Org Num'}, {key: 'date_first', disp: 'First Date'},
        // tslint:disable-next-line:max-line-length
        {key: 'date_entry', disp: 'Entry Date'}, {key: 'date_recent', disp: 'Recent Date'}, {key: 'date_exit', disp: 'Date Exit'}, {key: 'address_1', disp: 'Address Line'}, {key: 'city', disp: 'City'}, {key: 'zip', disp: 'Zip Code'},
        // tslint:disable-next-line:max-line-length
        {key: 'geo', disp: 'Geo Location'}, {key: 'pan', disp: 'Pan'}, {key: 'pin', disp: 'Pin'}]; // assuming data is consistent in the sample_data
      this.paginator();
      console.log(this.displayData);
    }, error => {
      console.log(error);
    });
  }

  paginator() {
    this.displayData =  this.data.slice((this.numberOfCols * (this.currentPage - 1)), (this.numberOfCols * (this.currentPage)));
  }
  getLength() {
    return Math.ceil(this.data.length / this.numberOfCols);
  }

  submit(val) {
    console.log(val);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    // @ts-ignore
    const options = { headers };
    this.http.post('example.com/getData', {id: val.id, status: val.status}, options ).subscribe(v => {
      console.log(v);
    //  do something here
    }, error => {
      console.log(error);
    });
  }


}
